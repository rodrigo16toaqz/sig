<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		$this->load->view('header'); //encabezado
		$this->load->view('welcome_message');// Cargando CONTENIDO
		$this->load->view('footer'); //piede PAGINA
	}

	public function universidad()
	{
		$this->load->view('header'); //encabezado
		$this->load->view('universidad');// Cargando CONTENIDO
		$this->load->view('footer'); //piede PAGINA
	}

	public function ciudad()
	{
		$this->load->view('header'); //encabezado
		$this->load->view('ciudad');// Cargando CONTENIDO
		$this->load->view('footer'); //piede PAGINA
	}
}
