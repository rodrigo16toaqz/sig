<br>
<h1>DIRECCION DE LA UTC</h1>
<img src="<?php echo base_url('assets/img/marcadorUTC.png'); ?>" alt="Logo UTC">
<br>
<div id="mapa1" style="width:100%; height:400PX; border:2px solid black;">

</div>

<script type="text/javascript">
<!--crear coordenada -->
    function initMap(){
      var coordenadaCentral=new google.maps.LatLng(-0.9177724406368407, -78.63188892582444)

      var miMapa=new google.maps.Map(
        document. getElementById('mapa1'),
        {
          center: coordenadaCentral,
          zoom: 9,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
      );
      var marcadorUTC= new google.maps.Marker({
        position:new google.maps.LatLng(-0.9178551607259913, -78.6329643425772),
        map:miMapa,
        title: 'UTC Matriz',
        icon:'<?php echo base_url('assets/img/marcadorUTC.png'); ?>'
      });
      var marcadorUTCSalache= new google.maps.Marker({
        position:new google.maps.LatLng(-0.9970111998422992, -78.62447446475542),
        map:miMapa,
        title: 'UTC Salache'
      });

      var marcadorUTCPujilí= new google.maps.Marker({
        position:new google.maps.LatLng(-0.9581592989637074, -78.69645603068622),
        map:miMapa,
        title: 'UTC Pujilí'
      });
      var marcadorUTCLaManá= new google.maps.Marker({
        position:new google.maps.LatLng(-0.9437690222453767, -79.23776141757716),
        map:miMapa,
        title: 'UTC La Maná',
        icon:'<?php echo base_url('assets/img/marcadorUTC.png'); ?>'
    });

      }

</script>
